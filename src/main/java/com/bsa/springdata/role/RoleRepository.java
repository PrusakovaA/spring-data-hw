package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    @Transactional
    @Modifying
    @Query("delete from Role r where r.users is empty and r.code =?1")
    void deleteRole(String roleCode);
}
