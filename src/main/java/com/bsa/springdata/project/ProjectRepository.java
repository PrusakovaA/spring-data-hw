package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query("select p from Project p inner join p.teams t on t.technology.name = ?1 " +
            "order by size(t.users)")
    List<Project> findTop5ByTechnology(String technology, PageRequest of);

    @Query("select p from Project p inner join p.teams t " +
            "inner join t.users u order by size(t) desc, " +
            "size(u) desc, p.name desc")
    List<Project> findTheBiggest(Pageable p);

    @Query("select count(distinct p) from Project p " +
            "inner join p.teams t inner join t.users u " +
            "inner join u.roles r on r.name = ?1")
    int getCountWithRole(String role);

    @Query(value = "select distinct p.name, " +
            "count(distinct t.id) as teamsNumber, " +
            "count(distinct u.id) as developersNumber, " +
            "string_agg(distinct tech.name, ',' order by tech.name desc) as technologies " +
            "from teams t " +
            "inner join users u on (t.id=u.team_id) " +
            "inner join technologies tech on (tech.id = t.technology_id) " +
            "inner join projects p on (p.id = t.project_id) " +
            "group by p.name order by p.name", nativeQuery = true)
    List<ProjectSummaryDto> getSummary();
}