package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

    int countByTechnologyName(String newTechnology);

    Optional<Boolean> findByName(String hipstersFacebookJavaScript);

    @Transactional
    @Modifying
    @Query("update Team t set t.technology = (select tech from Technology tech where tech.name = ?3)" +
            " where size(t.users) < ?1 and t in (select team from Team team where team.technology.name = ?2)")
    void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName);


    @Transactional
    @Modifying
    @Query(value = "update teams set name = CONCAT(name, '_', (select name from projects where id = teams.project_id), " +
            "'_', (select name from technologies where id = teams.technology_id)) where name = ?1", nativeQuery = true)
    void normalizeName(String hipsters);

}
