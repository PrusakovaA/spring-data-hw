package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {
    @Query("select distinct f from Office f inner join f.users u " +
            "inner join u.team t " +
            "inner join t.technology tech " +
            "where tech.name = ?1")
    List<Office> getByTechnology(String technology);

    @Modifying
    @Transactional
    @Query("update Office f set f.address = ?2 where f.address = ?1 " +
            "and size(f.users) > 0")
     void updateAddress(String oldAddress, String newAddress);

    Optional<Office> findByAddress(String newAddress);

}
